(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = global || self, global.dummy = factory());
}(this, function () { 'use strict';

    /**
     * Just a phony method for testing
     * @param {*} something pass anything
     * @return {*} return anything
     */
    function main(something) {
        console.log('You pass:', something);

        return something;
    }

    return main;

}));
//# sourceMappingURL=app.js.map
