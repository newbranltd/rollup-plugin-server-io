// rollup config for testing
// import rollup from 'rollup';
import { join } from 'path';
import serverIoCorePlugin from '../../src/index';
import buble from 'rollup-plugin-buble'

let config = {
  input: join(__dirname, 'src', 'main.js'),
  output: {
    file: join(__dirname, 'dist', 'app.js'),
    format: 'umd',
    name: 'dummy',
    sourcemap: true
  },
  plugins: [
    buble(),
    serverIoCorePlugin({
      webroot: [
        join(__dirname, 'dist')
      ],
      port: 3000
    })
  ],
  onwarn ({ code, message }) {
    if (code !== 'UNRESOLVED_IMPORT') {
      console.warn(message)
    }
  }
}

export default config;
