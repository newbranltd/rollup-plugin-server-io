/**
 * Just a phony method for testing
 * @param {*} something pass anything
 * @return {*} return anything
 */
export default function(something) {
    console.log('You pass:', something);

    return something;
}
