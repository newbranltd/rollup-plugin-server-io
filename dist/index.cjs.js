'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var serverIoCore = _interopDefault(require('server-io-core'));

/**
 * Our custom rollup plugin, a wrapper for server-io-core
 */
/**
 * @param {object} options pass to server-io-core
 * @return {object} for rollup
 */
function serverIo(options) {
  if ( options === void 0 ) options = {};

  // Force these options on config
  var config = Object.assign(options, {
    autoStart: false,
    reload: {
      enable: true,
      verbose: false
    }
  });
  // getting the fn(s)
  var ref = serverIoCore(config);
  var start = ref.start;
  var stop = ref.stop;
  // listen to signal and close it
  ['SIGINT', 'SIGTERM'].forEach(function (signal) {
    process.on(signal, function () {
      stop();
      process.exit();
    });
  });
  var running;
  // return
  return {
    name: 'serverIo',
    ongenerate: function ongenerate () {
      if (!running) {
        running = true;
        start();
      }
    }
  }
}

module.exports = serverIo;
