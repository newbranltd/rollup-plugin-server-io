# rollup-plugin-server-io

All-in-one dev server solution, with live reload, debugger, http proxy and a whole lot more.

## Installation

```sh
$ npm install rollup-plugin-server-io --save-dev
```

or using yarn

```sh
$ yarn add rollup-plugin-server-io --dev
```

## Try it

Clone this repo, run install, then `npm run demo`. The code is in the `tests/fixtures` folder, have fun.

## Example

```js
import rollup from 'rollup';
import serverIo from 'rollup-plugin-server-io'
import { join } from 'path'

let config = {
  input: './src/main.js',
  output: {
    file: './www/js/app.js',
    format: 'umd',
    name: 'App',
    sourcemap: true
  },
  plugins: [
    serverIo({
      // The only required field
      webroot: [
        join(__dirname, 'dist'),
        join(__dirname, 'assets')
      ],
    })
  ]
}

export default config
```

When you run rollup, it will automatically open your browser and point to your dev site.

## Configratuion

For full configuration options, please visit [server-io-core](https://gitlab.com/newbranltd/server-io-core) project page.

---

MIT - [NEWBRAN LTD OPEN SOURCE](https://gitlab.com/newbranltd) / [Joel Chu](https://joelchu.com)
